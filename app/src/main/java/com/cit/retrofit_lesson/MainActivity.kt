package com.cit.retrofit_lesson

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val retrofit = Retrofit.Builder()
            .baseUrl("http://strukov-artemii.online:8083/")
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val api: API = retrofit.create(API::class.java)

        api.getFilms().push(object: OnGetData<List<ModelFilm>>{
            override fun getData(model: List<ModelFilm>) {
                Log.e("tag", model.toString())
            }
        })

        api.getCatalog().push(object: OnGetData<List<ModelCatalog>>{
            override fun getData(model: List<ModelCatalog>) {
                Log.e("tag", model.toString())
            }
        })
    }

    private fun <T> Call<ModelResponse<T>>.push(callback: OnGetData<T>){
        enqueue(object : Callback<ModelResponse<T>>{
            override fun onResponse(
                call: Call<ModelResponse<T>>,
                response: Response<ModelResponse<T>>,
            ) {
                val answer = response.body()
                if (answer == null) {
                    Toast.makeText(this@MainActivity, "Body null", Toast.LENGTH_LONG).show()
                }else if(answer.codeResponse.code != 200){
                    Toast.makeText(this@MainActivity, answer.codeResponse.description, Toast.LENGTH_LONG).show()
                }else{
                    callback.getData(response.body()!!.body)
                }
            }

            override fun onFailure(call: Call<ModelResponse<T>>, t: Throwable) {
                Toast.makeText(this@MainActivity, t.message, Toast.LENGTH_LONG).show()
            }
        })
    }
}

interface OnGetData <T> {
    fun getData(model: T)
}