package com.cit.retrofit_lesson

data class CodeResponse(
    val code: Int,
    val description: String
)

data class ModelResponse <T> (
    val codeResponse: CodeResponse,
    val body: T
)


data class ModelFilm(
    val id: Int
)

data class ModelCatalog(
    val genre: Genre
)

data class Genre(
    val id: Int
)