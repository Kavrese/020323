package com.cit.retrofit_lesson

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface API {
    @GET("films")
    fun getFilms(): Call<ModelResponse<List<ModelFilm>>>

    @GET("catalog")
    fun getCatalog(): Call<ModelResponse<List<ModelCatalog>>>
}