package com.cit.retrofit_lesson

data class ModelPlant(
    val id: Int,
    val title: String
)
